# SALVANDO A ET

El siguiente proyecto consiste en un juego sencillo, desde un punto inicial el usuario deberá
llegar a una meta, y para ello, es necesario recorrer un camino generado aleatoriamente, 
utilizando para ello los siguientes cursores de movimiento: las teclas:
arrow.right, arrow.up, arrow.down y arrow.left (← , ↑ , ↓ , y →) respectivamente.


# Historia:

Salvando a ET es un juego escrito y desarrollado en conjunto por los autores Jorge Carrillo Silva, Yostin Sepúlveda Caniqueo y José Valenzuela Soto, utilizando para ello el lenguaje de programación Python, específicamente la versión 3.8.5
Este juego se guía con énfasis bajo las indicaciones de la PEP-8 para el código en su totalidad, como es el respetar la longitud de cada línea, indentar correctamente, entre otras.
contiene tres librerías, las cuales son: os, time e input_flechas.


# Para empezar:

Es requisito tener instalado Python3, de preferencia la versión 3.8.5 y está pensada que su ejecución sea si o sí en un entorno Linux, tales como Debian y sus derivados, etc.


# Instalación:

Para instalar y ejecutar el juego en su máquina, es necesario que siga las presentes indicaciones:

1- Clonar el repositorio https://gitlab.com/proyecto_elcamino/stable.git en el directorio de su preferencia.

2- Luego entrar en la carpeta clonada que lleva por nombre ElCamino_Final, que contiene 3 archivos y una carpeta, es esencial que pueda comprobar y cerciorarse que se encuentran los paquetes y librerías esenciales, ya que de lo contrario el juego podría no funcionar adecuadamente (pycache, input.py, SalvandoAET.py y  README.md)

3- Posteriormente, ya está usted preparado para ejecutar el programa. Para ello, abrirá una terminal (En caso de estar en Linux) y en el directorio que tenga los archivos, ejecutará el siguiente comando.
	python3 SalvandoAET.py

4- Ahora solo le queda divertirse! Mucho éxito!


# Codificación:

El juego fue pensado para soportar la codificación UTF-8.


# Construido con:

Atom: Uno de los 2 IDE`s utilizados para desarrollar el proyecto.
VSCodium: JUnto con Atom, es uno de los Entornos de desarrollo utilizados para el desarrollo del proyecto.


# Licencia:

Este proyecto está sujeto bajo la Licencia GNU GPL v3


# Autores y creadores del proyecto:

Jorge Carrillo Silva, Yostin Sepulveda Caniqueo y José Valenzuela Soto.


# Agradecimientos:

Queremos agradecer a todos aquellos que nos han ayudado y nos han orientado en el desarrollo del proyecto, en especial a nuestro profesor Fabio Durán y al ayudante Arturo Lobos.

