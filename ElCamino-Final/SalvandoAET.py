#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Llamar e incluir librerías necesarias
from input import input as input_flechas
import os
import random
import time


# Mapa base
linea02 = [
    [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 7],
    [2, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 7],
    [2, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 7],
    [2, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 7],
    [2, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 7],
    [2, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 7],
    [2, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 7],
    [2, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 7],
    [2, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 7],
    [2, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 7],
    [2, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 2, 7],
    [2, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 2, 7],
    [2, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 2, 7],
    [2, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 2, 7],
    [2, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 2, 7],
    [2, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 2, 7],
    [2, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 2, 7],
    [2, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 2, 7],
    [2, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 2, 7],
    [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 7]
]
# Almacenamos el mapa original


# Aquí definimos nuestras variables
r = 0
n = 0
pos_jugador_fila = 1      # Fila inicial donde estará el jugador
pos_jugador_columna = 1   # Columna inicial donde estará el jugador
ciclo_menu = 0
reset = 0
pasos = 0                 # Variable contadora de movimientos totales


# Definicion de funciones
def game_start():   # Función del menú inicial y el mapa
    os.system("clear")
    print("\t        👽 HOLA HUMANO ¿PUEDES AYUDARME A ENCONTRAR MI NAVE? 🛸\n")
    print("""\n\t INSTRUCCIONES DE JUEGO:\n
    El juego consiste en una simple y divertida dinámica, donde el objetivo
    es ayudar a E.T a encontrar su nave para poder volver a su mundo.
    E.T ha chocado en Rosswell y necesita de tu ayuda!!!
    Los cursores de movimiento son: las teclas ← , ↑ , ↓ , y → .
    \n\t Buena suerte humano!!!\n\n""")
    global ciclo_menu
    while ciclo_menu == 0:  # Variable que reincia el menú en casos dados
        print("""\t- Presiona 1 para ayudar a E.T a encontrar su nave!
        - Presiona 2 para alejarte sin mirar atrás.""")
        gameop = input("\n\n    >\t")  # El ingreso del dato del ususario
        if gameop == "1":   # Acción que realiza la computadora si pulsa 1:
            ciclo_menu = 1
            os.system("clear")  # Se limpia la pantalla, para dar paso al juego
            mapa()  # Creación del mapa aleatorio
            game()  # Activación de cursores y movimiento en general
            menu_final()  # Menú que aparecerá al final del juego

        elif gameop == "2":    # Acción que realiza la computadora si pulsa 2:
            ciclo_menu = 1
            print("\n\t Humanoo!! no te vayas!:c \n")

        else:   # Aviso y reinicio del menú en caso de ingresar dato inválido
            os.system("clear")
            print("""\n\t LA OPCIÓN INGRESADA NO ES VÁLIDA
            \t INTENTE DE NUEVO!\n\n""")
            print("\t👽 HOLA HUMANO ¿PUEDES AYUDARME A ENCONTRAR MI NAVE? 🛸")
            print("\n")


def menu_final():   # Función del menú de finalización y reinicio del juego
    global linea01
    global reset
    global n
    global pos_jugador_columna
    global pos_jugador_fila
    global pasos
    menuop = 0
    while menuop == 0:
        print("""\n Humano! ¿Deseas volver a intentarlo?:
            1.- Intentarlo otra vez.
            2.- Salir y decir adios... :c""")
        opcion = input("""\n   >\t""")  # Ingreso de dato por parte del usuario

        if opcion == "1":   # Acción que realiza la computadora si pulsa 1:
            reset = 0
            n = 0
            pasos = 0
            pos_jugador_fila = 1
            pos_jugador_columna = 1
            os.system("clear")
            mapa()
            game()

        elif opcion == "2":    # Acción que realiza la computadora si pulsa 2:
            menuop = 1
            print("\n\t Gracias por tu ayuda Humano!!!\n")
            exit

        else:   # Aviso y reinicio del menú en caso de ingresar dato inválido
            print("""\n\t     ESA OPCIÓN NO ES VÁLIDA
                \tINTENTE NUEVAMENTE!\n""")


def mapa():     # Función que generará caminos aleatorios en cada partida
    global reset
    global linea01
    global linea02
    if reset == 0:
        global r
        linea01 = [i[:] for i in linea02]
        for ciclo in range(20):
            for i in range(20):
                if linea01[ciclo][i] == 1:
                    r = (random.randint(0, 1))
                    if r == 0:
                        linea01[ciclo][i] = 0
                    else:
                        pass

            else:
                pass

    for ciclo in range(20):    # Ciclo que genera los límites y cursores
        for i in range(21):
            # Ciclo que generá al protagonista E.T
            if linea01[ciclo][i] == 3:
                print("👽", end="")
            # Ciclo que genera los límites del mapa (Árboles)
            elif linea01[ciclo][i] == 2 or linea01[ciclo][i] == 1:
                print("🌲", end="")
            # Ciclo que genera la meta del juego (Nave espacial)
            elif linea01[ciclo][i] == 4:
                print("🛸", end="")
            # Ciclo que genera gemas confundidoras
            elif linea01[ciclo][i] == 0:
                print("🔸", end="")
            # Ciclo que genera los espacios del mapa
            elif linea01[ciclo][i] == 7:
                print(" ")

    reset = 1
    ciclo = 0
    i = 0


def movimiento(linea01):
    global pos_jugador_fila
    global pos_jugador_columna
    global n
    global pasos
    tecla = input_flechas()
    # Ciclo que indica acción si pulsa ↑
    if tecla.name == "arrow up":
        if (linea01[pos_jugador_fila - 1][pos_jugador_columna] == 0 or
            linea01[pos_jugador_fila - 1][pos_jugador_columna] == 3 or
                linea01[pos_jugador_fila - 1][pos_jugador_columna] == 4):
            pos_jugador_fila = pos_jugador_fila - 1
            linea01[pos_jugador_fila][pos_jugador_columna] = 3
            linea01[pos_jugador_fila + 1][pos_jugador_columna] = 0
            pasos = pasos + 1
            os.system("clear")
            mapa()

    # Ciclo que genera acción si pulsa ↓
    if tecla.name == "arrow down":
        if (linea01[pos_jugador_fila + 1][pos_jugador_columna] == 0 or
            linea01[pos_jugador_fila + 1][pos_jugador_columna] == 3 or
                linea01[pos_jugador_fila + 1][pos_jugador_columna] == 4):
            pos_jugador_fila = pos_jugador_fila + 1
            linea01[pos_jugador_fila][pos_jugador_columna] = 3
            linea01[pos_jugador_fila - 1][pos_jugador_columna] = 0
            pasos = pasos + 1
            os.system("clear")
            mapa()

    # Ciclo que genera acción si pulsa ←
    if tecla.name == "arrow left":
        if (linea01[pos_jugador_fila][pos_jugador_columna - 1] == 0 or
            linea01[pos_jugador_fila][pos_jugador_columna - 1] == 3 or
                linea01[pos_jugador_fila][pos_jugador_columna - 1] == 4):
            pos_jugador_columna = pos_jugador_columna - 1
            linea01[pos_jugador_fila][pos_jugador_columna] = 3
            linea01[pos_jugador_fila][pos_jugador_columna + 1] = 0
            pasos = pasos + 1
            os.system("clear")
            mapa()

    # Ciclo que genera acción si pulsa →
    if tecla.name == "arrow right":
        if (linea01[pos_jugador_fila][pos_jugador_columna + 1] == 0 or
            linea01[pos_jugador_fila][pos_jugador_columna + 1] == 3 or
                linea01[pos_jugador_fila][pos_jugador_columna + 1] == 4):
            pos_jugador_columna = pos_jugador_columna + 1
            linea01[pos_jugador_fila][pos_jugador_columna] = 3
            linea01[pos_jugador_fila][pos_jugador_columna - 1] = 0
            pasos = pasos + 1
            os.system("clear")
            mapa()


def game():    # Función que muestra cantidad de pasos y agradecimiento
    global n
    global pasos
    while n == 0:
        movimiento(linea01)
        if pos_jugador_fila == 18 and pos_jugador_columna == 18:
            n = 1
            os.system("clear")
            print("\n\t ¡GRACIAS HUMANO, ME HAS SALVADO, ESTOY AGRADECIDO!!")
            print("\n En ", pasos, "pasos he escapado de esta situación.")


# Pantalla de carga y llamadas a las funciones time.sleep y game_start
print("\n\n\t                           LOADING.... \n\n")
time.sleep(2)
game_start()
